type SignalCallback = (
    data: unknown,
    signal_name: string,
    signalNotFromThisTab: boolean,
    slot_name: string,
) => void;

interface Subscriber {
    callback: SignalCallback;
    slot: string;
    priority: number;
    once?: boolean;
    signal: string | RegExp;
}

interface StringSubscriber extends Subscriber {
    signal: string;
}

interface RegexpSubscriber extends Subscriber {
    signal: RegExp;
}

export default class TabSignal {
    localStorageKey: string;
    stringSlots = new Map<string, StringSubscriber[]>();
    regexpSlots = new Map<RegExp, RegexpSubscriber[]>();

    sigId = 1000000;
    debug = false;

    /**
     * Constructor for TabSignal class
     *
     * @param instanceId - Instances with the same id can exchange signals between different tabs
     */
    constructor(instanceId: string) {
        this.localStorageKey = `TabSignal_storage_emit_${instanceId}`;

        window.addEventListener(
            'storage',
            (e) => {
                if (e.key && e.key === this.localStorageKey) {
                    try {
                        const data = JSON.parse(e.newValue as string);
                        if (data !== undefined && data.name !== undefined) {
                            if (this.debug) console.log(data);
                            this.emit(data.name, data.param, true);
                        }
                    } catch (failed) {
                        console.log(failed);
                    }
                }
            },
            false,
        );
    }

    /**
     * Subscribe to signal
     */
    connect(signal: string | RegExp, callback: SignalCallback, priority?: number): string | undefined {
        return this.on({ signal, callback, priority });
    }

    /**
     * Method to subscribe callback to signal
     *
     * @example
     * signals.on({
     *      signal:'event-name',
     *      slot:'slot-ABC',
     *      callback: (data, signalName) => { alert('ABC'); },
     *      priority:1,
     *      once: true
     * })
     *
     * @returns Slot name
     */
    on({
        signal,
        callback,
        slot = 'sig' + this.sigId++,
        priority = this.sigId++,
        once = false,
    }: {
        signal: string | RegExp;
        callback: SignalCallback;
        slot?: string;
        priority?: number;
        once?: boolean;
    }): string | undefined {
        let slots: Map<unknown, Subscriber[]>;

        if (signal instanceof RegExp) {
            slots = this.regexpSlots;
        } else if (typeof signal === 'string') {
            slots = this.stringSlots;
        } else {
            console.error('Unknown signal type');
            return;
        }

        if (!slots.has(signal)) slots.set(signal, []);
        const subscribers = slots.get(signal)!;
        subscribers.push({ callback, slot, priority, once, signal });

        subscribers.sort((a, b) => {
            const diff = a.priority - b.priority;
            return Number.isNaN(diff) ? 0 : diff;
        });

        return slot;
    }

    /**
     * Subscribe to signal and call callback only once, same as
     */
    once(signal_name: string, slot_function: SignalCallback) {
        return this.on({
            signal: signal_name,
            callback: slot_function,
            once: true,
        });
    }

    /**
     * Unsubscribe slot from signal
     *
     * @param {string} slot_name - Name of slot.
     * @param {(string|RegExp)} signal_name - Name of signal.
     * @returns {boolean} true if successfully unsubscribed, else false
     */
    disconnect(slot_name: string, signal_name: string | RegExp) {
        for (const subscribersMap of [this.stringSlots, this.regexpSlots]) {
            const subscribers = (subscribersMap as Map<unknown, Subscriber[]>).get(signal_name) || [];

            for (const [idx, val] of subscribers.entries()) {
                if (val.slot === slot_name) {
                    subscribers.splice(idx, 1);
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Method that returns string subscribers for signal and removes from original
     * map those who must be called once
     */
    private _stringSubscribers(signalName: string) {
        const stringSubscribers = this.stringSlots.get(signalName) || [];
        const stringSubscribersOriginal = stringSubscribers.slice();

        let idx = stringSubscribers.length;
        while (idx--) {
            if (stringSubscribers[idx].once) {
                stringSubscribers.splice(idx, 1);
            }
        }

        return stringSubscribersOriginal;
    }

    /**
     * Method that returns string subscribers for signal and removes from original
     * map those who must be called once
     */
    private _regexpSubscribers(signalName: string) {
        const subscribers = [];

        for (const signalRegexp of this.regexpSlots.keys()) {
            if (signalName.match(signalRegexp) === null) {
                continue;
            }

            const regexSubscribers = this.regexpSlots.get(signalRegexp)!;

            subscribers.push(...regexSubscribers);

            let idx = regexSubscribers.length;
            while (idx--) {
                if (regexSubscribers[idx].once) {
                    regexSubscribers.splice(idx, 1);
                }
            }
        }

        return subscribers;
    }

    /**
     * Calls subscribed on signal callbacks
     *
     * @param signalName - Signal name.
     * @param param - Data to be passed to callback as first argument.
     * @param signalNotFromThisTab - True value means that signal come from another tab.
     * @param fail - Fail if one of subscribers will throw an error.
     */
    emit(signalName: string, param?: unknown, signalNotFromThisTab = false, fail = false) {
        signalNotFromThisTab = signalNotFromThisTab === true;

        const invokeCallbacks = (subscribers: Subscriber[]) => {
            for (let i = 0; i < subscribers.length; i++) {
                const subscriber = subscribers[i];

                if (typeof subscriber.callback !== 'function') return;

                if (this.debug || fail) {
                    subscriber.callback(param, signalName, signalNotFromThisTab, subscriber.slot);
                } else {
                    try {
                        subscriber.callback(param, signalName, signalNotFromThisTab, subscriber.slot);
                    } catch (exception) {
                        console.warn('Error in emit signal ' + signalName, exception);
                    }
                }
            }
        };

        invokeCallbacks(this._stringSubscribers(signalName));
        invokeCallbacks(this._regexpSubscribers(signalName));
    }

    generateRandomId() {
        return (
            Math.random() +
            '_' +
            Math.random() +
            '_' +
            Math.random() +
            '_' +
            Math.random() +
            '_' +
            Math.random()
        );
    }

    /**
     * Emit signal to all tabs using local storage
     */
    emitAll(signalName: string, data?: unknown) {
        this.emit(signalName, data);

        try {
            if (window.localStorage !== undefined) {
                window.localStorage[this.localStorageKey] = JSON.stringify({
                    name: signalName,
                    custom_id: this.generateRandomId(),
                    param: data,
                });
            }
            return true;
        } catch (e) {
            return false;
        }
    }
}
