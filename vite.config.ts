import { resolve } from 'path';
import { defineConfig } from 'vite';

export default defineConfig({
    build: {
        target: 'es2015',
        lib: {
            entry: resolve(__dirname, 'src/tabsignal.ts'),
            name: 'tabsignal',
        },
    },
});
