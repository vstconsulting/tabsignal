const TabSignal = require('./dist/tabsignal.umd.js');

global.window = { addEventListener: () => {} };

const signals = new TabSignal('testSignals1');

let i = 0;

const signalNames = [];

for (let i = 0; i < 10 ** 2; i++) {
    signalNames.push('signal' + i);
}

for (let signalName of signalNames) {
    signals.on({ signal: signalName, callback: () => i++ });
    // signals.on({ signal: new RegExp('^' + signalName + '$'), callback: () => i++ });
}

for (let signalName of signalNames) {
    for (let j = 0; j < 10 ** 5; j++) {
        signals.emit(signalName);
    }
}

console.log(i);
