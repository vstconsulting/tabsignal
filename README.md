[![pipeline status](https://gitlab.com/vstconsulting/tabsignal/badges/master/pipeline.svg)](https://gitlab.com/vstconsulting/tabsignal/-/commits/master)
[![coverage report](https://gitlab.com/vstconsulting/tabsignal/badges/master/coverage.svg)](https://gitlab.com/vstconsulting/tabsignal/-/commits/master)
[![npm version](https://badge.fury.io/js/%40vstconsulting%2Ftabsignal.svg)](https://badge.fury.io/js/%40vstconsulting%2Ftabsignal)

# TabSignal

Simple library for emitting and receiving signals.


# Projects

* Polemarch (https://polemarch.org/)
* VSTUtils (https://gitlab.com/vstconsulting/vstutils)


# License

TabSignal is licensed under the terms of the MIT.
See the file "LICENSE" for more information.

Copyright 2020 VST Consulting.
