module.exports = {
    root: true,
    extends: ['eslint:recommended', 'plugin:@typescript-eslint/recommended', 'plugin:prettier/recommended'],
    parser: '@typescript-eslint/parser',
    plugins: ['@typescript-eslint'],
    env: {
        browser: true,
        commonjs: true,
        es6: true,
    },
    rules: {
        'prettier/prettier': 'error',
        '@typescript-eslint/no-var-requires': 'off',
    },
};
