/* eslint-env jest */

const TabSignal = require('./dist/tabsignal.umd.js');

// eslint-disable-next-line no-unused-vars
const subscriber = (data, signal_name, signalNotFromThisTab, slot_name) => undefined;

const DATA = { a: 1, b: 2, c: 3 };

const SIGNAL1 = 'signal1';
const SIGNAL2 = 'signal2';

test('emit signal', () => {
    const signals = new TabSignal('testSignals1');

    // Check invalid input
    expect(signals.on({ signal: 123, callback: () => {} })).toBe(undefined);
    expect(signals.disconnect('no slot', 'no name')).toBe(false);

    let signal1_subscriber1 = jest.fn(subscriber);
    let signal1_subscriber2 = jest.fn(subscriber);
    let signal2_subscriber1 = jest.fn(subscriber);

    const signal1_subscriber1_slot = signals.on({
        slot: 'slot1',
        priority: 1,
        signal: SIGNAL1,
        callback: signal1_subscriber1,
    });

    expect(signal1_subscriber1_slot).toBe('slot1');

    const signal1_subscriber2_slot = signals.on({
        signal: SIGNAL1,
        callback: signal1_subscriber2,
        once: true,
    });

    signals.on({
        signal: SIGNAL2,
        callback: signal2_subscriber1,
    });

    signals.emit(SIGNAL1, DATA);

    expect(signal1_subscriber1).toHaveBeenCalledTimes(1);
    expect(signal1_subscriber2).toHaveBeenCalledTimes(1);
    expect(signal2_subscriber1).toHaveBeenCalledTimes(0);

    expect(signal1_subscriber1).toHaveBeenLastCalledWith(DATA, SIGNAL1, false, signal1_subscriber1_slot);
    expect(signal1_subscriber2).toHaveBeenLastCalledWith(DATA, SIGNAL1, false, signal1_subscriber2_slot);

    expect(signals.disconnect(signal1_subscriber1_slot, SIGNAL1)).toBe(true);

    signals.emit(SIGNAL1, DATA);

    expect(signal1_subscriber1).toHaveBeenCalledTimes(1);
    expect(signal1_subscriber2).toHaveBeenCalledTimes(1);
    expect(signal2_subscriber1).toHaveBeenCalledTimes(0);
});

test('regexp subscribers', () => {
    const REGEXP_SIGNAL1 = /signal<\d{4}>/;

    const signals = new TabSignal('testSignals2');

    const subscriber1 = jest.fn(subscriber);
    const subscriber2 = jest.fn(subscriber);
    const subscriber3 = jest.fn(subscriber);

    const signal1_subscriber1_slot = signals.on({
        signal: REGEXP_SIGNAL1,
        callback: subscriber1,
    });

    signals.on({
        signal: /Signal/,
        callback: subscriber2,
    });

    signals.on({
        signal: REGEXP_SIGNAL1,
        callback: subscriber3,
        once: true,
    });

    signals.emit('signal<1337>', DATA);
    signals.emit('signal<1>', DATA);

    expect(subscriber1).toHaveBeenCalledTimes(1);
    expect(subscriber2).toHaveBeenCalledTimes(0);
    expect(subscriber3).toHaveBeenCalledTimes(1);

    expect(subscriber1).toHaveBeenLastCalledWith(DATA, 'signal<1337>', false, signal1_subscriber1_slot);

    signals.disconnect(signal1_subscriber1_slot, REGEXP_SIGNAL1);

    signals.emit('signal<1337>', DATA);

    expect(subscriber1).toHaveBeenCalledTimes(1);
    expect(subscriber2).toHaveBeenCalledTimes(0);
    expect(subscriber3).toHaveBeenCalledTimes(1);
});

test('errors in callback', () => {
    const signals = new TabSignal('testSignals3');

    signals.on({
        signal: 'signalWithError',
        callback: () => {
            throw new TypeError();
        },
    });

    expect(() => signals.emit('signalWithError', undefined, false, true)).toThrow(TypeError);
    expect(() => signals.emit('signalWithError', undefined, false, false)).not.toThrow(TypeError);
});

test('subscribers execution order', () => {
    const signals = new TabSignal('testSignals3');

    const SIGNAL = 'Signal1';
    const SIGNAL_REGEXP = /Signal1/;

    const result = [];

    const createSubscriber = (i) => () => result.push(i);

    const subscriber1 = jest.fn(createSubscriber(1));
    const subscriber2 = jest.fn(createSubscriber(2));
    const subscriber3 = jest.fn(createSubscriber(3));

    const subscriber4 = jest.fn(createSubscriber(4));
    const subscriber5 = jest.fn(createSubscriber(5));
    const subscriber6 = jest.fn(createSubscriber(6));

    signals.on({ signal: SIGNAL, callback: subscriber2, priority: 2 });
    signals.on({ signal: SIGNAL, callback: subscriber3, priority: 3 });
    signals.on({ signal: SIGNAL, callback: subscriber1, priority: 1 });

    signals.on({ signal: SIGNAL_REGEXP, callback: subscriber6, priority: 3 });
    signals.on({ signal: SIGNAL_REGEXP, callback: subscriber4, priority: 1 });
    signals.on({ signal: SIGNAL_REGEXP, callback: subscriber5, priority: 2 });

    signals.emit(SIGNAL);

    expect(result).toEqual([1, 2, 3, 4, 5, 6]);
});
